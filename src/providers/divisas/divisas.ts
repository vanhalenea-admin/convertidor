import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Country } from './../../app/models/Country';

/*
  Generated class for the DivisasProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DivisasProvider {

  private URL_BASE = 'https://free.currencyconverterapi.com/api/v6';
  private COUNTRIES_URL= this.URL_BASE + '/countries';
  private COUNTRIES_FLAG_URL= 'https://www.countryflags.io';
 // https://free.currencyconverterapi.com/api/v6/convert?q=USD_PHP&compact=y
  

  constructor(public http: HttpClient) {
 
  }

getAllCountryData(){
   return this.http.get<Country>(this.COUNTRIES_URL);
}

getCountryFlagMedium(countryId : string){

  return this.COUNTRIES_FLAG_URL+'/'+countryId+'/flat/48.png';


}

}
