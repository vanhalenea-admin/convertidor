import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { IndexPage } from './index';
import { DivisasProvider } from './../../providers/divisas/divisas';

@NgModule({
  declarations: [
    IndexPage,
  ],
  imports: [
    IonicPageModule.forChild(IndexPage),
  ],
  providers:[
    DivisasProvider
  ]
})
export class IndexPageModule {}
