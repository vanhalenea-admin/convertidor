import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DivisasProvider } from './../../providers/divisas/divisas';
import { Observable } from 'rxjs';
import {  Country } from './../../app/models/Country';

/**
 * Generated class for the IndexPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-index',
  templateUrl: 'index.html',
})
export class IndexPage {

  countries$ : any;
  paises : any;
  paisesMostrar : Array<Country> = [];

  constructor(public navCtrl: NavController, public navParams: NavParams,public divisasProvider: DivisasProvider) {
  }

  ionViewDidLoad() {
    this.getAllCountries();
          /*
              this.countries$.subscribe(
              data =>  console.log(data),
              err => console.log(err),
              () => console.log('completed!')
          ); 
            */ 

  }


  getAllCountries(){

    this.divisasProvider.getAllCountryData().subscribe(countries => {
        this.countries$ = countries;
        this.paises = this.countries$.results;

        Object.keys(this.paises).forEach(key=> {
          //console.log(this.paises[key]);
          let obj = this.paises[key];
          let pais  = new Country();

          pais.currencyId = obj.currencyId;
          pais.currencyName = obj.currencyName;
          pais.id = obj.id;
          pais.flag_img =this.divisasProvider.getCountryFlagMedium(pais.id);

          this.paisesMostrar.push(pais);


      });

    });

      

  }
  
}
